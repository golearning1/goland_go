package main

import "fmt"

func main(){
	var tanlov int

	fmt.Println("Qaysi faslning oylari kerak\n1: Qish\n2: Bahor\n3: Yoz\n4: Kuz")
	fmt.Println("Tanlang")
	fmt.Scanln(&tanlov)
	switch tanlov{
	case 1:
		fmt.Println("1: Qish faslini oylari\nDekabr\nYanvar\nFevral")
	case 2:
		fmt.Println("2: Bahor faslini oylari\nMart\nAprel\nMay")
	case 3:
		fmt.Println("3: Yoz faslini oylari\nIyun\nIyul\nAvgust")
	case 4:
		fmt.Println("4: Kuz faslini oylari\nSentabr\nOktabr\nNoyabr")
	default:
		fmt.Println("4 ta yil fasli bor xolos")
	}
}